// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { toast } from "react-toastify";
import { addDoc, collection, getFirestore} from 'firebase/firestore';
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyAzx1Yc3O2zE3loZqPSVoZiEef0n6pJ9Ds",
    authDomain: "project-animal-invertory.firebaseapp.com",
    projectId: "project-animal-invertory",
    storageBucket: "project-animal-invertory.appspot.com",
    messagingSenderId: "4335133012",
    appId: "1:4335133012:web:47350f36b922713774e8c1",
    measurementId: "G-4YHVGG7TM4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
//const functions = getFunctions(app);
const storage = getStorage(app);

const registerWithEmailAndPassword = async (email, password, name, numberP) => {
    try {
      await createUserWithEmailAndPassword(auth, email, password).then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        addDoc(collection(db, "users"), {
          uid: user.uid,
          name,
          rol:"cliente",
          email,
          status:"Habilitado",
        });
        toast.success("Registrado con exitoso");
        console.log(user.uid)
        // backend(user);
  
        // ...
      })
    } catch (error) {
      
      toast.error("Error al iniciar sesión");
      console.log(error);
    }
};


export { db, auth, storage, registerWithEmailAndPassword };
