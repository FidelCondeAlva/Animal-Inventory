import React,{useState } from 'react'
import { useNavigate } from "react-router-dom"; 
//firebase
import { auth } from '../firebaseConfing/firebaseConfig';
import { signInWithEmailAndPassword, createUserWithEmailAndPassword } from "firebase/auth";
//React-bootstrap
import { Form, Button } from "react-bootstrap"
import "./Login.css"
function Login() {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  let navigate = useNavigate(); // servicio para cargar otros componentes mediante las URL -> navigate(path)

  function singIn(){
    try{
      signInWithEmailAndPassword(auth, email, password).then((userCredential) => {
        const user = userCredential.user;
        console.log("Bienvenido: ", user.uid)
        navigate("/homePage")
      }).catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(error.message)  
      }); 
    }catch(e){
      console.log("error")
    }
    
  }

  return (
    <div className='login-container'>
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="ejemplo@ejemplo.com" onChange={e => setEmail(e.target.value)} required/>
        <Form.Text className="text-muted">
         No compartiremos su email con terceros. 
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Contraseña</Form.Label>
        <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Recordar usuario" />
      </Form.Group>
      <Button variant="primary"onClick={singIn}>
        Iniciar Sesión
      </Button>
    </Form>
    </div>
  )
}

export default Login