import './App.css';
import Login from './components/login/Login';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navigate,Route, Router, Routes, Outlet } from "react-router-dom";
import HomePage from './components/homePage/HomePage';
import About from './components/about/About';
import Profile from './components/profile/Profile';
import AnimalControl from './components/animalControl/AnimalControl';

function App() {
  return (
    <div className="App">
        <Routes>
          <Route exact path="/homePage"  element={<HomePage />} />
          <Route exact path="/profile"  element={<Profile />} />
          <Route exact path="/about"  element={<About />} />
          <Route exact path="/login"  element={<Login />} />
          <Route exact path="/animal-control"  element={<AnimalControl />} />
        </Routes>
    </div>
  );
}

export default App;
